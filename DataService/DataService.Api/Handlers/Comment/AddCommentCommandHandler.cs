﻿using DataService.Abstractions.Commands.Comment;
using DataService.Db;
using DataService.Model;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace DataService.Api.Handlers.Post
{
    public class AddCommentCommandHandler : IRequestHandler<AddCommentCommand, AddCommentResponse>
    {
        private readonly ICommentRepository commentRepository;

        public AddCommentCommandHandler(ICommentRepository commentRepository)
        {
            this.commentRepository = commentRepository;
        }

        public async Task<AddCommentResponse> Handle(AddCommentCommand request, CancellationToken cancellationToken)
        {
            ValidationHelper.ValidateDateTime(request.CommentDate);

            Comment comment = new Comment() { Text = request.Text, PostId = request.PostId, CommentDate = request.CommentDate };
            long result = await commentRepository.Add(comment).ConfigureAwait(false);
            return new AddCommentResponse() { Id = result };
        }
    }
}
