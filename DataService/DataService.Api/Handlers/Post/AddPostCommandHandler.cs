﻿using DataService.Abstractions.Commands.Post;
using DataService.Db;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace DataService.Api.Handlers.Post
{
    public class AddPostCommandHandler : IRequestHandler<AddPostCommand, AddPostResponse>
    {
        private readonly IPostRepository postRepository;

        public AddPostCommandHandler(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public async Task<AddPostResponse> Handle(AddPostCommand request, CancellationToken cancellationToken)
        {
            ValidationHelper.ValidateDateTime(request.PostDate);

            Model.Post post = new Model.Post() {Text = request.Text, Title = request.Title, PostDate = request.PostDate };
            long result = await postRepository.Add(post).ConfigureAwait(false);
            return new AddPostResponse() { Id = result};
        }
    }
}
