﻿using DataService.Abstractions.Queries.Post;
using DataService.Db;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace DataService.Api.Handlers.Post
{
    public class GetPostByIdCommandHandler : IRequestHandler<GetPostByIdQuery, GetPostByIdResponse>
    {
        private readonly IPostRepository postRepository;

        public GetPostByIdCommandHandler(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public async Task<GetPostByIdResponse> Handle(GetPostByIdQuery request, CancellationToken cancellationToken)
        {
            Model.Post result = await postRepository.GetById(request.Id).ConfigureAwait(false);
            return new GetPostByIdResponse(result);
        }
    }
}
