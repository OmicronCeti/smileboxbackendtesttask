﻿using DataService.Abstractions.Queries.Post;
using DataService.Db;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DataService.Api.Handlers.Post
{
    public class GetAllPostsCommandHandler : IRequestHandler<GetAllPostsQuery, GetAllPostsResponse>
    {
        private readonly IPostRepository postRepository;

        public GetAllPostsCommandHandler(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public async Task<GetAllPostsResponse> Handle(GetAllPostsQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<Model.Post> results = await postRepository.GetAll().ConfigureAwait(false);
            return new GetAllPostsResponse(results);
        }
    }
}
