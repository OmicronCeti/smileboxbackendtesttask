﻿using DataService.Abstractions;
using DataService.Abstractions.Commands.Post;
using DataService.Db;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace DataService.Api.Handlers.Post
{
    public class RemovePostCommandHandler : IRequestHandler<RemovePostCommand, MessageBase>
    {
        private readonly IPostRepository postRepository;

        public RemovePostCommandHandler(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public async Task<MessageBase> Handle(RemovePostCommand request, CancellationToken cancellationToken)
        {
            await postRepository.Remove(request.Id);
            return new GenericResponse();
        }
    }
}
