﻿using System;

namespace DataService.Api.Handlers
{
    public static class ValidationHelper
    {

        public static void ValidateDateTime(DateTime dateTime)
        {
            bool valid = false;

            try
            {
                valid = DateTime.Now.ToUniversalTime() > dateTime.ToUniversalTime();
            }
            catch(Exception e)
            {
                throw new ValidationException($"Incorrect post date.", e);
            }

            if (!valid)
            {
                throw new ValidationException($"Incorrect post date.");
            }
        }
    }
}
