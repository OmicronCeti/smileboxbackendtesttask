﻿using DataService.Api.Controllers;
using DataService.Db;
using DataService.Db.Impl;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataService.Api
{
    public static class Bootstrapper
    {
        public static void Init(IServiceCollection services, IConfiguration configuration)
        {
            Configure(services, configuration);
            Register(services);
        }

        private static void Configure(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.Configure<DatabaseSettings>(configuration.GetSection("DatabaseSettings"));
        }

        private static void Register(IServiceCollection serviceCollection)
        {
            serviceCollection.AddMediatR(typeof(PostsController).Assembly);
            serviceCollection.AddSingleton<IPostRepository, PostRepository>();
            serviceCollection.AddSingleton<ICommentRepository, CommentRepository>();
            serviceCollection.AddSingleton<IDatabaseContextFactory, DatabaseContextFactory>();
        }
    }
}
