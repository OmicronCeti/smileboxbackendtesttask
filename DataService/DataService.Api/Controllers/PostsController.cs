﻿using System.Net;
using System.Threading.Tasks;
using DataService.Abstractions;
using DataService.Abstractions.Commands.Post;
using DataService.Abstractions.Queries.Post;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DataService.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : ControllerBase
    {
        private readonly IMediator mediator;

        public PostsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET api/values
        [HttpGet("[action]")]
        [ProducesResponseType(typeof(OperationResult<GetAllPostsResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllPostsQuery request)
        {
            GetAllPostsResponse response = await mediator.Send(request).ConfigureAwait(false);

            return Ok(OperationResult<GetAllPostsResponse>.Ok(response));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(OperationResult<GetPostByIdResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById([FromRoute]GetPostByIdQuery request)
        {
            GetPostByIdResponse response = await mediator.Send(request).ConfigureAwait(false);

            return Ok(OperationResult<GetPostByIdResponse>.Ok(response));
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResult<AddPostResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Add([FromBody]AddPostCommand request)
        {
            AddPostResponse response = await mediator.Send(request).ConfigureAwait(false);
            return Ok(OperationResult<AddPostResponse>.Ok(response));
        }

        [HttpDelete]
        [ProducesResponseType(typeof(OperationResult<object>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromBody]RemovePostCommand request)
        {
            await mediator.Send(request).ConfigureAwait(false);
            return Ok(OperationResult<object>.Ok());
        }
    }
}