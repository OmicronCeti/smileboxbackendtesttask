﻿using System.Net;
using System.Threading.Tasks;
using DataService.Abstractions;
using DataService.Abstractions.Commands.Comment;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DataService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly IMediator mediator;

        public CommentsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(OperationResult<AddCommentResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Add([FromBody]AddCommentCommand request)
        {
            AddCommentResponse response = await mediator.Send(request).ConfigureAwait(false);
            return Ok(OperationResult<AddCommentResponse>.Ok(response));
        }
    }
}