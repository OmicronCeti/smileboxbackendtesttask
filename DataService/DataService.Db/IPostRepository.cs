﻿using DataService.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataService.Db
{
    public interface IPostRepository
    {
        Task<long> Add(Post post);
        Task Remove(long id);
        Task<IEnumerable<Post>> GetAll();
        Task<Post> GetById(long id);
    }
}