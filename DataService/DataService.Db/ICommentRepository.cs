﻿using System.Threading.Tasks;
using DataService.Model;

namespace DataService.Db
{
    public interface ICommentRepository
    {
        Task<long> Add(Comment comment);
    }
}