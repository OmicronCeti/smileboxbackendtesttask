﻿using DataService.Model;
using System.Threading.Tasks;

namespace DataService.Db.Impl
{
    public sealed class CommentRepository : ICommentRepository
    {
        private readonly IDatabaseContextFactory factory;

        public CommentRepository(IDatabaseContextFactory factory)
        {
            this.factory = factory;
        }

        public async Task<long> Add(Comment comment)
        {
            using (DatabaseContext context = factory.Context)
            {
                await context.AddAsync(comment).ConfigureAwait(false);
                context.SaveChanges();
            }

            return comment.Id;
        }
    }
}
