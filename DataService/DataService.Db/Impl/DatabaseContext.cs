﻿using Microsoft.EntityFrameworkCore;
using DataService.Model;

namespace DataService.Db.Impl
{
    public class DatabaseContext : DbContext
    {
        private readonly DatabaseSettings settings;

        public DatabaseContext(DatabaseSettings settings)
        {
            this.settings = settings;
        }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>()
                .Property(p => p.Id)
                .IsRequired();

            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Post)
                .WithMany(p => p.Comments)
                .HasForeignKey(c => c.PostId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($"Data Source={settings.Ip};Initial Catalog={settings.Database};persist security info=True;user id={settings.Login};password={settings.Password}");
        }
    }
}
