﻿using Microsoft.Extensions.Options;

namespace DataService.Db.Impl
{
    public class DatabaseContextFactory : IDatabaseContextFactory
    {
        private readonly DatabaseSettings settings;

        public DatabaseContextFactory(IOptions<DatabaseSettings> options)
        {
            settings = options.Value;
        }

        public DatabaseContext Context { get { return new DatabaseContext(settings);} }

    }
}
