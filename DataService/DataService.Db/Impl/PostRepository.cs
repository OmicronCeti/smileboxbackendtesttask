﻿using DataService.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataService.Db.Impl
{
    public sealed class PostRepository : IPostRepository
    {
        private readonly IDatabaseContextFactory factory;

        public PostRepository(IDatabaseContextFactory factory)
        {
            this.factory = factory;
        }

        public async Task<long> Add(Post post)
        {
            using (DatabaseContext context = factory.Context)
            {
                await context.AddAsync(post).ConfigureAwait(false);
                context.SaveChanges();
            }

            return post.Id;
        }

        public async Task Remove(long id)
        {
            using (DatabaseContext context = factory.Context)
            {
                // Only Entity Framwork Plus allows Delte with condition. Otherwise we are quering object first to check if can delete.
                context.Posts.Remove(await factory.Context.Posts.Include(post => post.Comments).FirstAsync(p => p.Id == id && p.Comments.Count == 0).ConfigureAwait(false));
                context.SaveChanges();
            }
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            return await factory.Context.Posts.Include(post => post.Comments).OrderBy(p => p.PostDate).ToListAsync().ConfigureAwait(false);
        }

        public async Task<Post> GetById(long id)
        {
            return await factory.Context.Posts.Include(post => post.Comments).FirstAsync(p => p.Id == id).ConfigureAwait(false);
        }
    }
}
