﻿using DataService.Db.Impl;

namespace DataService.Db
{
    public interface IDatabaseContextFactory
    {
        DatabaseContext Context { get; }
    }
}