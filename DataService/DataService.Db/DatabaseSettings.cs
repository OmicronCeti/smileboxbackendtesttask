﻿namespace DataService.Db
{
    /// <summary>
    /// Class for handing database settings such as connection string and datatabase name.
    /// </summary>
    public sealed class DatabaseSettings
    {
        /// <summary>
        /// Gets or sets ip address of db
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Gets or sets name of database for connecting to
        /// </summary>
        public string Database { get; set; }

        /// <summary>
        /// Gets or sets login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets timeout milliseconds for connecting to database
        /// </summary>
        public int ConnectionTimeoutMilliseconds { get; set; }
    }
}
