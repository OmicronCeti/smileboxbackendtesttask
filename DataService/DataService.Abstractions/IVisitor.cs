﻿using DataService.Abstractions.Commands.Comment;
using DataService.Abstractions.Commands.Post;
using DataService.Abstractions.Queries.Post;
using System.Threading.Tasks;

namespace DataService.Abstractions
{
    public interface IVisitor
    {
        Task<GetAllPostsQuery> Visit(GetAllPostsQuery request);

        Task<GetPostByIdQuery> Visit(GetPostByIdQuery request);

        Task<AddPostCommand> Visit(AddPostCommand request);

        Task<RemovePostCommand> Visit(RemovePostCommand request);

        Task<AddCommentCommand> Visit(AddCommentCommand request);
    }
}
