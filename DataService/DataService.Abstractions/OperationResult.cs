﻿namespace DataService.Abstractions
{
    public sealed class OperationResult<TResult> : IOperationResult<TResult>
    {
        private static readonly OperationResult<TResult> OkInstance = new OperationResult<TResult>(OkResult, null, default(TResult));

        public const string OkResult = "Ok";
        public const string FailResult = "Fail";

        public OperationResult(string result, string message, TResult data)
        {
            Result = result;
            Message = message;
            Data = data;
        }

        public string Result { get; }

        public string Message { get; }

        public TResult Data { get; }

        public static OperationResult<TResult> Ok() => OkInstance;

        public static OperationResult<TResult> Ok(TResult data) => new OperationResult<TResult>(OkResult, null, data);

        public static OperationResult<TResult> Fail(string message) => new OperationResult<TResult>(FailResult, message, default(TResult));
    }
}
