﻿using MediatR;
using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataService.Abstractions.Commands.Post
{
    public sealed class AddPostCommand : MessageBase, IRequest<AddPostResponse>, IVisitable
    {
        [Required]
        [JsonProperty]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [JsonProperty]
        [StringLength(2147483647)]
        public string Text { get; set; }

        [Required]
        [JsonProperty]
        public DateTime PostDate { get; set; }

        public override string Type => nameof(AddPostCommand);

        public async Task<MessageBase> Accept(IVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException(nameof(visitor));
            }

            return await visitor.Visit(this).ConfigureAwait(false);
        }
    }
}
