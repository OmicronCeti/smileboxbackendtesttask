﻿namespace DataService.Abstractions.Commands.Post
{
    public sealed class AddPostResponse : MessageBase
    {
        public long Id;

        public override string Type => nameof(AddPostResponse);
    }
}
