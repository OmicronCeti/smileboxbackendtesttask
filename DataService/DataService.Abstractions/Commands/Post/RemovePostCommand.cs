﻿using MediatR;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DataService.Abstractions.Commands.Post
{
    public sealed class RemovePostCommand : MessageBase, IRequest<MessageBase>, IVisitable
    {
        [JsonProperty]
        [Required(AllowEmptyStrings = false)]
        public long Id { get; set; }

        public override string Type => nameof(RemovePostCommand);

        public async Task<MessageBase> Accept(IVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException(nameof(visitor));
            }

            return await visitor.Visit(this).ConfigureAwait(false);
        }
    }
}
