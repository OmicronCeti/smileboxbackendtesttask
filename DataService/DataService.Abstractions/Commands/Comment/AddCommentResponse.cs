﻿namespace DataService.Abstractions.Commands.Comment
{
    public sealed class AddCommentResponse : MessageBase
    {
        public long Id;

        public override string Type => nameof(AddCommentResponse);
    }
}
