﻿using MediatR;
using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DataService.Abstractions.Commands.Comment
{
    public sealed class AddCommentCommand : MessageBase, IVisitable, IRequest<AddCommentResponse>
    {
        [Required]
        [JsonProperty]
        public long PostId { get; set; }

        [Required]
        [JsonProperty]
        [StringLength(2147483647)]
        public string Text { get; set; }

        [Required]
        [JsonProperty]
        public DateTime CommentDate { get; set; }

        public override string Type => nameof(AddCommentCommand);

        public async Task<MessageBase> Accept(IVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException(nameof(visitor));
            }

            return await visitor.Visit(this).ConfigureAwait(false);
        }
    }
}
