﻿using MediatR;
using System;
using System.Threading.Tasks;

namespace DataService.Abstractions.Queries.Post
{
    public sealed class GetAllPostsQuery : MessageBase, IRequest<GetAllPostsResponse>, IVisitable
    {
        public GetAllPostsQuery()
        {

        }

        public override string Type => nameof(GetAllPostsQuery);

        public async Task<MessageBase> Accept(IVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException(nameof(visitor));
            }

            return await visitor.Visit(this).ConfigureAwait(false);
        }
    }
}
