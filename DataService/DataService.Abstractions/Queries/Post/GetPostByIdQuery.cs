﻿using MediatR;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DataService.Abstractions.Queries.Post
{
    public sealed class GetPostByIdQuery : MessageBase, IRequest<GetPostByIdResponse>, IVisitable
    {
        public GetPostByIdQuery()
        {

        }
        public GetPostByIdQuery(long id)
        {
            Id = id;
        }

        [Required]
        [JsonProperty]
        public long Id { get; set; }

        public override string Type => nameof(GetPostByIdQuery);

        public async Task<MessageBase> Accept(IVisitor visitor)
        {
            if (visitor == null)
            {
                throw new ArgumentNullException(nameof(visitor));
            }

            return await visitor.Visit(this).ConfigureAwait(false);
        }
    }
}
