﻿using System.Collections.Generic;

namespace DataService.Abstractions.Queries.Post
{
    public sealed class GetAllPostsResponse : MessageBase
    {
        public GetAllPostsResponse()
        {

        }

        public GetAllPostsResponse(IEnumerable<Model.Post> posts)
        {
            Posts = posts;
        }

        public IEnumerable<Model.Post> Posts { get; set; }

        public override string Type => nameof(GetAllPostsResponse);
    }
}
