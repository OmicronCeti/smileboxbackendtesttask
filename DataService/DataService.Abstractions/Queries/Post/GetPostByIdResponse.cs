﻿namespace DataService.Abstractions.Queries.Post
{
    public sealed class GetPostByIdResponse : MessageBase
    {
        public GetPostByIdResponse()
        {

        }

        public GetPostByIdResponse(Model.Post post)
        {
            Post = post;
        }

        public Model.Post Post { get; set; }

        public override string Type => nameof(GetPostByIdResponse);
    }
}
