﻿namespace DataService.Abstractions
{
    public abstract class MessageBase : IMessageBase
    {
        protected MessageBase()
        {

        }

        /// <summary>
        /// Gets the type of the message
        /// </summary>
        public abstract string Type { get; }

        /// <summary>
        /// Gets or sets data related to specific type of message
        /// </summary>
        public object Data { get; set; }
    }
}
