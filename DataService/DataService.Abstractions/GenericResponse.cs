﻿namespace DataService.Abstractions
{
    public sealed class GenericResponse : MessageBase
    {
        public string Error { get; set; }

        /// <inheritdoc/>
        public override string Type => nameof(GenericResponse);
    }
}
