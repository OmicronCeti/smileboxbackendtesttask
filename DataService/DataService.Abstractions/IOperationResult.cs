﻿namespace DataService.Abstractions
{
    public interface IOperationResult<out TResult>
    {
        /// <summary>
        /// Gets result of operation (Ok or Fail)
        /// </summary>
        string Result { get; }

        /// <summary>
        /// Message related to operation (error description in case of failing)
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Content of the result
        /// </summary>
        TResult Data { get; }
    }
}
