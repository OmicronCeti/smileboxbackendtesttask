﻿using System.Threading.Tasks;

namespace DataService.Abstractions
{
    public interface IVisitable
    {
        Task<MessageBase> Accept(IVisitor visitor);
    }
}
