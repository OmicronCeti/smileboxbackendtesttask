﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataService.Model
{
    public sealed class Post
    {
        [Key]
        [Required]
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public DateTime PostDate { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
