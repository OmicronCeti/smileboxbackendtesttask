﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataService.Model
{
    public sealed class Comment
    {
        [Key]
        [Required]
        public long Id { get; set; }
        [Required]
        [ForeignKey("Id")]
        public long PostId { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public DateTime CommentDate { get; set; }
        public Post Post { get; set; }
    }
}
